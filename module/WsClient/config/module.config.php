<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
return array(
    'router' => array(
        'routes' => array(
            'rWsClient' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/WsClient',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'WsClient\Controller',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'rsWsClientChild' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/[:controller]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z0-9_-]+',
                            ),
                            'defaults' => array(
                                '__NAMESPACE__' => 'WsClient\Controller',
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'controllers'=> array(
      'invokables' => array(
          'Connection' => 'WsClient\Controller\ConnectionController',
          'Reservation' => 'WsClient\Controller\ReservationController',
      ),
    ),
    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
);
