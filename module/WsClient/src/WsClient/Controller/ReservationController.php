<?php

namespace WsClient\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use WsClient\Model\Reservation;
use WsClient\Model\ReservationQuery;
//use Zend\View\Model\JsonModel\UtilisateurQuery;
//use \WsClient\Model\ClientQuery;
use Zend\Session\Container;
use Zend\View\Model\JsonModel;

class ReservationController extends AbstractRestfulController {
    
    public function create($InfoReservation){
        
        $container = new Container('utilisateur');
        $client = $container->client;
        
        $reservation = new Reservation();
        $reservation->setUtilisateur($client);
        $reservation->setEtat("demande");
        $reservation->setDatereservation(date("Y-m-d H:i:s"));
        $reservation->setDateprevuestockage($InfoReservation["dateDebut"]);
        $reservation->setNbjoursdestockageprevu($InfoReservation["quantite"]);
        $reservation->setQuantite($InfoReservation["quantite"]);
        $reservation->save();
        
        return new JsonModel(array(
            'Réservation effectuée',
            'Récapitulatif' => var_dump($reservation),
        ));
    }
    
    public function getList(){
        $container = new Container('utilisateur');
        $client = $container->client;
        $reservation = ReservationQuery::create()->findByArray(array(
            'numClient' => $client->getNumutilisateur()
        ));
        $resultat = $reservation->toArray();
        return new JsonModel(array(
            $resultat
        ));
    }
    
    public function get($id){
        $resultat = null;
        $reservation = ReservationQuery::create()->findPk($id);
        if($reservation !=null){
            $resultat = $reservation->toArray();
        }
        return new JsonModel(array(
            $resultat
        ));
    }
    
     public function delete($id) {
        $resultat = false;
        $reservation = ReservationQuery::create()->findPk($id);
        if($reservation!=null){
            $resultat = $reservation->delete();
            $resultat = true;
        }
        return new JsonModel(array(
            "Suppression" => $resultat
        ));
    }
    
}